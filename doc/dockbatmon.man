.TH DOCKBATMON 1 "July 2013" "dockbatmon" "User's Manual"

.SH NAME
dockbatmon \- Dockable battery monitor

.SH SYNOPSIS
.B dockbatmon
[\fIOPTION\fR]...

.SH DESCRIPTION
\fBdockbatmon\fR is a dockable application to monitor the capacity and charge
of a laptop battery for the WindowMaker window manager.

.SH "OPTIONS"

.TP
\fB\-d\fR
display debugging messages.

.TP
\fB\-h\fR
display usage and exit

.TP
\fB\-v\fR
output version information and exit

.SH CREDITS
\fBdockbatmon\fR was written by Hugo Villeneuve <hugo@hugovil.com>.

.SH COPYRIGHT
\fBdockbatmon\fR is free; anyone may redistribute it to anyone under the terms
stated in the GNU General Public License. A copy of the license is included in
the \fBdockbatmon\fR distribution. You can also browse it online at
.I http://www.gnu.org/copyleft/gpl.html
