/*
 * options.c -- functions for processing command-line options and arguments
 *
 * Copyright (C) 2005 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#if HAVE_CONFIG_H
#  include "config.h"
#endif

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#if STDC_HEADERS
#  include <string.h>
#elif HAVE_STRINGS_H
#  include <strings.h>
#endif

#include "common.h"
#include "main.h"
#include "options.h"

/*******************************************************************************
 * Display the help message and exit
 ******************************************************************************/
static void DisplayUsage(void)
{
	printf("Usage: %s [OPTIONS]...\n", PACKAGE_NAME);
	printf("Dockable battery monitor.\n\n");
	printf("  -d                        Display debugging messages.\n");
	printf("  -h                        display this help and exit\n");
	printf("  -version                  display version information");
	printf(" and exit\n");
	printf("\n");
}

/*******************************************************************************
 * Display version information and exit
 ******************************************************************************/
static void DisplayVersion(void)
{
	printf("\n");
	printf("  %s, version %s\n", PACKAGE_NAME, PACKAGE_VERSION);
	printf("  Written by Hugo Villeneuve\n\n");
}

static void InvalidOption(char *message, /*@null@ */ char *string)
{
	if (string == NULL)
		fprintf(stderr, "%s: %s\n", PACKAGE_NAME, message);
	else
		fprintf(stderr, "%s: %s %s\n", PACKAGE_NAME, message, string);

	fprintf(stderr, "Try `%s -h' for more information.\n", PACKAGE_NAME);

	exit(EXIT_FAILURE);
}

/*******************************************************************************
 * Initializes the different options passed as arguments on the command line.
 ******************************************************************************/
void ParseCommandLineOptions(int argc, char *argv[])
{
	int i;
	char *token;

	/* Default values. */
	batmon_infos.debug = false;

	for (i = 1; i < argc; i++) {
		token = argv[i];
		switch (token[0]) {
		case '-':
			/* Processing options names */
			switch (token[1]) {
			case 'd':
				batmon_infos.debug = true;
				break;
			case 'h':
				DisplayUsage();
				exit(EXIT_SUCCESS);
			case 'v':
				if (STREQ("version", &token[1])) {
					DisplayVersion();
					exit(EXIT_SUCCESS);
				}
				InvalidOption("invalid option", token);
				break;
			default:
				InvalidOption("invalid option", token);
				break;
			}	/* end switch( token[1] ) */
			break;
		default:
			/* Processing options arguments */
			InvalidOption("invalid option", token);
			break;
		}		/* end switch( token[0] ) */
	}			/* end for */
}
