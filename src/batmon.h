/*
 * batmon.h
 *
 * Copyright (C) 2013 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef BATMON_H
#define BATMON_H 1

#include <gtk/gtk.h>

void
batmon_init(GtkWidget *win);

#endif /* BATMON_H */
