/*
 * main.c -- main program
 *
 * Copyright (C) 2005 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

/* Define filename_M */
#define MAIN_M 1

#if HAVE_CONFIG_H
#  include "config.h"
#endif

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <assert.h>

#include "common.h"
#include "dockapp.h"
#include "options.h"
#include "batmon.h"
#include "main.h"

/*******************************************************************************
 * Main function
 ******************************************************************************/
int main(int argc, char *argv[])
{
	/* Initialization */
	ParseCommandLineOptions(argc, argv);

	/* Initializing and creating a DockApp window. */
	dockapp_init(argc, argv);

	gtk_main();

	return EXIT_SUCCESS;
}
