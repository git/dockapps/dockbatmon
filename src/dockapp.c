/*
 * dockapp.c -- routines for managing dockapp windows and icons.
 *
 * Copyright (C) 2005 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Code for GTK withdraw ack taken from wmudmount
 * http://wmudmount.sourceforge.net/
 * Copyright © 2010  Brad Jorsch <anomie@users.sourceforge.net>
 *
 * This file is released under the GPLv2
 */

/* Define filename_M */
#define DOCKAPP_M 1

#if HAVE_CONFIG_H
#  include "config.h"
#endif

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include "common.h"
#include "batmon.h"
#include "main.h"
#include "dockapp.h"

#define GTK_WITHDRAWN_HACK 1

/* Width and height in pixels of Window Maker icons. */
#define ICON_SIZE 64

#ifdef GTK_WITHDRAWN_HACK

static void
dockapp_set_withdrawn_state(Display *d, Window mw, Window iw)
{
	XWMHints *hints;

	hints = XGetWMHints(d, mw);
	if (!hints) {
		hints = XAllocWMHints();
	}
	hints->flags |= IconWindowHint;
        hints->icon_window = iw;
	XSetWMHints(d, mw, hints);
	XFree(hints);
}

/*
 * Gtk/Gdk gives us no way to specify intial_state = WithdrawnState,
 * because ICCCM doesn't list it in the little table for initial_state.
 * And it even overrides it just before mapping the window!
 * See https://bugzilla.gnome.org/show_bug.cgi?id=139322
 *
 * But there is a hack: an X window isn't actually mapped until its
 * parent is mapped. So if we reparent the troublesome window to an
 * unmapped window and call GTK's map function, we can then easily
 * change the hints before reparenting it back to the root window.
 */
static void
dockapp_gtk_withdrawn_hack(GtkWidget *dockwin, GtkWidget *iconwin)
{
	Display *d;
	Window p, root, *children;
	unsigned int nchildren;
	Window win_temp;
	Window win_orig;
        Window iw;

	d = GDK_DISPLAY_XDISPLAY(gdk_display_get_default());
	win_orig = GDK_WINDOW_XID(gtk_widget_get_window(dockwin));
	iw = GDK_WINDOW_XID(gtk_widget_get_window(iconwin));

	XQueryTree(d, win_orig, &root, &p, &children, &nchildren);
	if (children)
		XFree(children);

	win_temp = XCreateSimpleWindow(d, p, 0, 0, 1, 1, 0, 0, 0);
	XReparentWindow(d, win_orig, win_temp, 0, 0);
	gtk_widget_show(dockwin);
        gtk_widget_show(iconwin);

	dockapp_set_withdrawn_state(d, win_orig, iw);

	XReparentWindow(d, win_orig, p, 0, 0);
	XDestroyWindow(d, win_temp);
}

#endif /* GTK_WITHDRAWN_HACK */

void
dockapp_set_mask(GtkWidget *iconwin, char **xpm)
{
	GdkPixbuf *pixbuf;
#if defined (HAVE_GTK2)
	GdkPixmap *pixmap;
	GdkBitmap *mask;
#elif defined (HAVE_GTK3)
	cairo_surface_t *surface;
	cairo_region_t *shape;
#endif

	pixbuf = gdk_pixbuf_new_from_xpm_data((const char **)xpm);
	if (!pixbuf) {
		fprintf(stderr, "Could not load pixbuf\n");
		exit(1);
	}

#if defined (HAVE_GTK2)
	gdk_pixbuf_render_pixmap_and_mask(pixbuf, &pixmap, &mask, 127);
	if (!pixmap) {
		fprintf(stderr, "Could not load pixmap\n");
		exit(1);
	}

	gtk_widget_shape_combine_mask(iconwin, mask, 0, 0);
	g_object_unref(mask);
	g_object_unref(pixmap);
#elif defined (HAVE_GTK3)
	surface = gdk_cairo_surface_create_from_pixbuf(
		pixbuf, 0,
		gtk_widget_get_window(iconwin));
	shape = gdk_cairo_region_create_from_surface(surface);
	cairo_surface_destroy(surface);
	gtk_widget_shape_combine_region(iconwin, shape);
	cairo_region_destroy(shape);
#endif
	g_object_unref(pixbuf);
}

static GtkWidget *
dockapp_win_init(GtkWindowType window_type)
{
	GtkWidget *win;

	win = gtk_window_new(window_type);
	gtk_window_set_wmclass(GTK_WINDOW(win), g_get_prgname(), "DockApp");
	gtk_widget_set_size_request(win, ICON_SIZE, ICON_SIZE);
	gtk_widget_set_app_paintable(win, true);
	gtk_widget_realize(win);

	return win;
}

/*******************************************************************************
 * New window creation and initialization for a Dockable Application
 ******************************************************************************/
void
dockapp_init(int argc, char *argv[])
{
	GtkWidget *dockwin;
	GtkWidget *iconwin;

	gtk_init(&argc, &argv);

	dockwin = dockapp_win_init(GTK_WINDOW_TOPLEVEL);
	iconwin = dockapp_win_init(GTK_WINDOW_POPUP);

	gtk_window_set_default_size(GTK_WINDOW(dockwin), ICON_SIZE, ICON_SIZE);
	g_signal_connect(dockwin, "destroy", G_CALLBACK(gtk_main_quit), NULL);

	/* Sets the icon of dockwin as iconwin. */
#if defined (HAVE_GTK2)
	gdk_window_set_icon(gtk_widget_get_window(dockwin),
			    gtk_widget_get_window(iconwin), NULL, NULL);
#endif

	batmon_init(iconwin);
	gtk_widget_show(iconwin);

#ifdef GTK_WITHDRAWN_HACK
	dockapp_gtk_withdrawn_hack(dockwin, iconwin);
#else
	gtk_widget_show(dockwin);
	gdk_window_withdraw(gtk_widget_get_window(dockwin));
#endif
}
