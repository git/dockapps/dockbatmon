/*
 * batmon.c -- functions for displaying battery level
 *
 * Copyright (C) 2005 Hugo Villeneuve <hugo@hugovil.com>
 *
 * Based on EggClockFace
 * (c) 2005, Davyd Madeley <davyd@madeley.id.au>
 *
 * This file is released under the GPLv2
 */

/* Define filename_M */
#define BATMON_M 1

#if HAVE_CONFIG_H
#  include "config.h"
#endif

#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#if STDC_HEADERS
#  include <string.h>
#elif HAVE_STRINGS_H
#  include <strings.h>
#else
#  error "Needs <string.h> or <strings.h>"
#endif
#include <math.h>

#include <time.h>
#include <sys/types.h>
#include <gtk/gtk.h>

#include "common.h"
#include "batmon.h"
#include "main.h"
#include "dockapp.h"
#include "dockapp-mask.xpm"

#define BAT_BORDER 2
#define BAT_W 35
#define BAT_H 49
#define BAT_TIP_W (BAT_W / 2)
#define BAT_TIP_H 5

#define BAT_START_X ((64 / 2) - (BAT_W / 2))
#define BAT_START_Y ((64 / 2) - ((BAT_H + BAT_TIP_H) / 2))

#define BAT_LOW_LEVEL 20
#define BAT_CRITICAL_LEVEL 10

#define FONT_NAME "Sans Condensed Bold"
#define FONT_SIZE 9

/* #define DEBUG_ACPI 1 */

static int font_size;
static int capacity;
static int ac_online = false;
static const char battery[] = "BAT0";
static const char ac_acpi_dir_base[] = "/sys/class/power_supply";
static const char battery_acpi_dir_base[] = "/sys/class/power_supply";

static int
read_int_from_file(const char *path)
{
	FILE *fp;
	int value;

	if (access(path, F_OK ) < 0) {
		/* File not found, abort. */
		fprintf(stderr, "Missing file.\n");
		exit(EXIT_FAILURE);
	}

	fp = fopen(path, "r");
	if (fp == NULL) {
		/* File could not be read, abort. */
		perror(path);
		fprintf(stderr, "Unable to read file.\n");
		exit(EXIT_FAILURE);
	} else {
		char buf[40];

		memset(buf, 0, sizeof(buf));
		fgets(buf, sizeof(buf), fp);
		sscanf(buf, "%d", &value);

#ifdef DEBUG_ACPI
		printf("value = %d\n", value);
#endif /* DEBUG_ACPI */
	}

	fclose(fp);

	return value;
}

static void read_ac_status(void)
{
	char path[256];

	sprintf(path, "%s/%s/online", ac_acpi_dir_base, "AC");

	if (access(path, F_OK ) < 0) {
		sprintf(path, "%s/%s/online", ac_acpi_dir_base, "AC0");

		if (access(path, F_OK ) < 0) {
			perror(path);
			fprintf(stderr,
				"Unable to find power supply AC online status.\n");
			exit(EXIT_FAILURE);
		}
	}

	ac_online = read_int_from_file(path);

#ifdef DEBUG_ACPI
	printf("ac online = %d\n", ac_online);
#endif /* DEBUG_ACPI */
}

static void
compute_battery_capacity_from_energy(void)
{
	char path[256];
	int64_t full, now;

	sprintf(path, "%s/%s/energy_full", battery_acpi_dir_base, battery);
#ifdef DEBUG_ACPI
	printf("Energy full: ");
#endif /* DEBUG_ACPI */
	full = read_int_from_file(path);

	sprintf(path, "%s/%s/energy_now", battery_acpi_dir_base, battery);
#ifdef DEBUG_ACPI
	printf("Energy now:  ");
#endif /* DEBUG_ACPI */
	now = read_int_from_file(path);

	capacity = (now * 100) / full;

#ifdef DEBUG_ACPI
	printf("capacity = %d%%\n", capacity);
#endif /* DEBUG_ACPI */
}

static void read_battery_capacity(void)
{
	char path[256];

	sprintf(path, "%s/%s/capacity", battery_acpi_dir_base, battery);

	if (access(path, F_OK ) < 0) {
		/* Capacity file doesn't exist, try reading energy... */
		compute_battery_capacity_from_energy();
	} else {
		capacity = read_int_from_file(path);
	}
}

static void
color_set_black(cairo_t *cr)
{
	cairo_set_source_rgb(cr, 0, 0, 0);
}

static void
color_set_white(cairo_t *cr)
{
	cairo_set_source_rgb(cr, 1, 1, 1);
}

static void
color_set_gray(cairo_t *cr)
{
	const float g = 0.3;

	cairo_set_source_rgb(cr, g, g, g);
}

static void
color_set_red(cairo_t *cr)
{
	cairo_set_source_rgb(cr, 1, 0, 0);
}

static void
color_set_green(cairo_t *cr)
{
	cairo_set_source_rgb(cr, 0, 1, 0);
}

static void
color_set_blue(cairo_t *cr)
{
	cairo_set_source_rgb(cr, 0, 0, 1);
}

static void
color_set_yellow(cairo_t *cr)
{
	cairo_set_source_rgb(cr, 1, 1, 0);
}

static void
display_string_pango(cairo_t *cr, char *str, int center_x, int center_y)
{
	PangoLayout *layout;
	PangoFontDescription *desc;
	int x, y;
	char font[256];

	sprintf(font, "%s %d", FONT_NAME, font_size);

	/* Create a PangoLayout, set the font and text */
	layout = pango_cairo_create_layout(cr);
	pango_layout_set_text(layout, str, -1);
	desc = pango_font_description_from_string(font);
	pango_layout_set_font_description(layout, desc);
	pango_font_description_free(desc);

	/* Get string dimensions */
	pango_layout_get_pixel_size(layout, &x, &y);

	cairo_move_to(cr, center_x + 1 - (x / 2), center_y - (y / 2));

	pango_cairo_show_layout(cr, layout);

	g_object_unref(layout);
}

static void
display_capacity(cairo_t *cr)
{
	char str[256];
	int center_x = 32;
	int center_y = 32;

	font_size = FONT_SIZE;

	/* Convert digit to string */
	if (capacity >= 100)
		font_size -= 1;

	sprintf(str, "%d%%", capacity);

	if (capacity <= 40) {
		color_set_white(cr);
		center_y -= 12;
	} else {
		color_set_black(cr);
		center_y += 15;
	}

	display_string_pango(cr, str, center_x, center_y);
}

static void
draw_background(cairo_t *cr)
{
	int y_start;
	int height;

	read_ac_status();
	read_battery_capacity();

	cairo_save(cr);

	cairo_set_line_width(cr, BAT_BORDER);

	/* Draw battery tip. */
	color_set_gray(cr);
	cairo_rectangle(cr, BAT_START_X + ((BAT_W - BAT_TIP_W) / 2),
			BAT_START_Y,
			BAT_TIP_W, BAT_TIP_H);
	cairo_fill_preserve(cr);
 	color_set_black(cr);
	cairo_stroke(cr);

	/* Draw battery outline. */
	color_set_gray(cr);
	cairo_rectangle(cr, BAT_START_X, BAT_START_Y + BAT_TIP_H,
			BAT_W, BAT_H);
	cairo_fill_preserve(cr);
 	color_set_black(cr);
	cairo_stroke(cr);

	if (ac_online) {
		color_set_blue(cr);
	} else if (capacity <= BAT_CRITICAL_LEVEL) {
		color_set_red(cr);
	} else if (capacity <= BAT_LOW_LEVEL) {
		color_set_yellow(cr);
	} else {
		color_set_green(cr);
	}

	y_start = BAT_START_Y + BAT_TIP_H + BAT_H;

	/* Compute colored height (no borders). */
	height = ((BAT_H - BAT_BORDER) * capacity) / 100;

	/* Make sure capacity > 0 gets a colored line. */
	if ((capacity != 0) && (height == 0))
		height = 1;

	/* Add border width: */
	height += BAT_BORDER;

	/* Draw remaining capacity. */
	cairo_rectangle(cr, BAT_START_X, y_start, BAT_W, -height);
	cairo_fill_preserve(cr);
 	color_set_black(cr);
	cairo_stroke(cr);

	display_capacity(cr);

	cairo_restore(cr);
}

static gboolean
#if defined (HAVE_GTK2)
batmon_expose(GtkWidget *w, GdkEventExpose *event)
#elif defined (HAVE_GTK3)
batmon_handler(GtkWidget *w, cairo_t *cr, gpointer d G_GNUC_UNUSED)
#endif
{
#if defined (HAVE_GTK2)
	cairo_t *cr;
#elif defined (HAVE_GTK3)
	GdkRectangle rect;
#endif

#if defined (HAVE_GTK2)
	/* Get a cairo_t */
	cr = gdk_cairo_create(gtk_widget_get_window(w));

	cairo_rectangle(cr, event->area.x, event->area.y,
		event->area.width, event->area.height);
#elif defined (HAVE_GTK3)
	gdk_cairo_get_clip_rectangle(cr, &rect);
	printf("Redrawing (%d,%d+%d+%d)\n", rect.x, rect.y,
	       rect.width, rect.height);

	cairo_rectangle(cr, rect.x, rect.y, rect.width, rect.height);
	cairo_clip(cr);
#endif

	draw_background(cr);

#if defined (HAVE_GTK2)
	cairo_destroy(cr);
#endif

	return TRUE;
}

static void
batmon_redraw_canvas(GtkWidget *widget)
{
	GdkRegion *region;

	if (!widget->window)
		return;

	region = gdk_drawable_get_clip_region(widget->window);
	/* redraw the cairo canvas completely by exposing it */
	gdk_window_invalidate_region(widget->window, region, TRUE);
	gdk_window_process_updates(widget->window, TRUE);

	gdk_region_destroy(region);
}

static gboolean
batmon_button_release(GtkWidget *w, GdkEventButton *event)
{
	(void) event; /* Unused parameter. */
	(void) w; /* Unused parameter. */

	if (batmon_infos.debug)
		printf("%s: button released\n", PACKAGE_NAME);

	return FALSE;
}

static gboolean
dockapp_update(gpointer data)
{
	GtkWidget *w = GTK_WIDGET(data);

	batmon_redraw_canvas(w);

	return TRUE; /* keep running this event */
}

void
batmon_init(GtkWidget *win)
{
	gtk_widget_add_events(win, GDK_BUTTON_PRESS_MASK |
			      GDK_BUTTON_RELEASE_MASK |
			      GDK_POINTER_MOTION_MASK);

#if defined (HAVE_GTK2)
	g_signal_connect(G_OBJECT(win), "expose-event",
			 G_CALLBACK(batmon_expose), NULL);
#elif defined (HAVE_GTK3)
	g_signal_connect(G_OBJECT(win), "draw",
			 G_CALLBACK(draw_handler), NULL);
#endif
	g_signal_connect(G_OBJECT(win), "button-press-event",
			 G_CALLBACK(batmon_button_release), NULL);
	g_signal_connect(G_OBJECT(win), "button-release-event",
			 G_CALLBACK(batmon_button_release), NULL);

	dockapp_set_mask(win, dockapp_mask_xpm);

	/* update the battery status once per second */
	g_timeout_add(1000, dockapp_update, win);

	batmon_redraw_canvas(win);
}
