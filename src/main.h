/*
 * main.h
 *
 * Copyright (C) 2005 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef MAIN_H
#define MAIN_H 1

struct batmon_t {
	int debug;
};

/* Exported variables */
#undef _SCOPE_
#ifdef BATMON_M
#  define _SCOPE_ /**/
#else
#  define _SCOPE_ extern
#endif

_SCOPE_ struct batmon_t batmon_infos;

#endif /* MAIN_H */
