/*
 * dockapp.h
 *
 * Copyright (C) 2005 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef DOCKAPP_H
#define DOCKAPP_H 1

#include <gtk/gtk.h>

void
dockapp_init(int argc, char *argv[]);

void
dockapp_set_mask(GtkWidget *iconwin, char **xpm);

#endif /* DOCKAPP_H */
