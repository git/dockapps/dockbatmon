/*
 * options.h
 *
 * Copyright (C) 2005 Hugo Villeneuve <hugo@hugovil.com>
 *
 * This file is released under the GPLv2
 */

#ifndef OPTIONS_H
#define OPTIONS_H 1

void
ParseCommandLineOptions(int argc, char *argv[]);

#endif /* OPTIONS_H */
